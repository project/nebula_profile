<?php

namespace Drupal\nebula_profile_content;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\media\Entity\Media;

/**
 * Defines a helper class for importing default content.
 *
 * @internal
 *   This code is only for use by the Umami demo: Content module.
 */
class InstallHelper implements ContainerInjectionInterface
{

    /**
     * The path alias manager.
     *
     * @var \Drupal\Core\Path\AliasManagerInterface
     */
    protected $aliasManager;

    /**
     * Entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * Module handler.
     *
     * @var \Drupal\Core\Extension\ModuleHandlerInterface
     */
    protected $moduleHandler;

    /**
     * State.
     *
     * @var \Drupal\Core\State\StateInterface
     */
    protected $state;

    /**
     * Constructs a new InstallHelper object.
     *
     * @param \Drupal\Core\Path\AliasManagerInterface $aliasManager
     *   The path alias manager.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
     *   Entity type manager.
     * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
     *   Module handler.
     * @param \Drupal\Core\State\StateInterface $state
     *   State service.
     */
    public function __construct(AliasManagerInterface $aliasManager, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, StateInterface $state)
    {
        $this->aliasManager = $aliasManager;
        $this->entityTypeManager = $entityTypeManager;
        $this->moduleHandler = $moduleHandler;
        $this->state = $state;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('path.alias_manager'),
            $container->get('entity_type.manager'),
            $container->get('module_handler'),
            $container->get('state')
        );
    }

    /**
     * Imports default contents.
     */
    public function importContent()
    {
        $this->importMedia()->importPages();
    }

    /**
     * Imports pages.
     *
     * @return $this
     */
    protected function importPages()
    {
        if (($handle = fopen($this->moduleHandler->getModule('nebula_profile_content')->getPath() . '/default_content/pages.csv', "r")) !== FALSE) {
            $headers = fgetcsv($handle);
            $uuids = [];
            while (($data = fgetcsv($handle)) !== FALSE) {
                $data = array_combine($headers, $data);

                // Prepare content.
                $values = [
                    'type' => 'page',
                    'title' => $data['title'],
                    'moderation_state' => 'published',
                ];

                $data['body'] = str_replace('UUID', $this->getMediaUuid(), $data['body']);

                // Fields mapping starts.
                // Set Body Field.
                if (!empty($data['body'])) {


                    // Create single new paragraph
                    $paragraph = Paragraph::create([
                        'type' => 'wysiwyg',
                        'field_wysiwyg_body' => ['value' => $data['body'], 'format' => 'wysiwyg'],
                    ]);
                    $paragraph->save();

                    $values['field_paragraphs'] = [[
                        'target_id' => $paragraph->id(),
                        'target_revision_id' => $paragraph->getRevisionId(),
                    ]];
                }

                // Create Node.
                $node = $this->entityTypeManager->getStorage('node')->create($values);
                $node->save();
                $uuids[$node->uuid()] = 'node';

                // Add a menu link to the main menu.
                $menu_link = MenuLinkContent::create([
                    'title' => $node->label(),
                    'link' => ['uri' => 'internal:/node/' . $node->id()],
                    'menu_name' => $data['menu'],
                    'expanded' => TRUE,
                ]);
                $menu_link->save();

            }
            $this->storeCreatedContentUuids($uuids);
            fclose($handle);
        }
        return $this;
    }


    public function importMedia() {
        $module_path = $this->moduleHandler->getModule('nebula_profile_content')->getPath();
        $counter = 0;
        foreach(file_scan_directory($module_path . '/default_content/images','/\.jpg/') as $key => $file) {
            $counter++;
            $image = $this->createFileEntity($file->uri);
            $media_image = Media::create([
                'bundle' => 'image',
                'name' => $file->name,
                'field_media_image' => [
                    'target_id' => $image,
                ],
            ]);
            $media_image->save();

            $this->storeMediaUuids($media_image->uuid());
        }

        return $this;
    }

    /**
     * Creates a file entity based on an image path.
     *
     * @param string $path
     *   Image path.
     *
     * @return int
     *   File ID.
     */
    protected function createFileEntity($path) {
        $uri = $this->fileUnmanagedCopy($path);
        $file = $this->entityTypeManager->getStorage('file')->create([
            'uri' => $uri,
            'status' => 1,
        ]);
        $file->save();
        $this->storeCreatedContentUuids([$file->uuid() => 'file']);
        return $file->id();
    }

    /**
     * Deletes any content imported by this module.
     *
     * @return $this
     */
    public function deleteImportedContent()
    {
        $uuids = $this->state->get('nebula_profile_content_uuids', []);
        $by_entity_type = array_reduce(array_keys($uuids), function ($carry, $uuid) use ($uuids) {
            $entity_type_id = $uuids[$uuid];
            $carry[$entity_type_id][] = $uuid;
            return $carry;
        }, []);
        foreach ($by_entity_type as $entity_type_id => $entity_uuids) {
            $storage = $this->entityTypeManager->getStorage($entity_type_id);
            $entities = $storage->loadByProperties(['uuid' => $entity_uuids]);
            $storage->delete($entities);
        }
        return $this;
    }



    /**
     * Stores record of content entities created by this import.
     *
     * @param array $uuids
     *   Array of UUIDs where the key is the UUID and the value is the entity
     *   type.
     */
    protected function storeCreatedContentUuids(array $uuids)
    {
        $uuids = $this->state->get('nebula_profile_content_uuids', []) + $uuids;
        $this->state->set('nebula_profile_content_uuids', $uuids);
    }

    protected function storeMediaUuids($uuid)
    {
        $uuids = $this->state->get('nebula_profile_media_uuids', []) + [$uuid];
        $this->state->set('nebula_profile_media_uuids', $uuids);
    }


    protected function getMediaUuid() {
        $uuids = $this->state->get('nebula_profile_media_uuids');
        return $uuids[0];
    }
    /**
     * Wrapper around file_unmanaged_copy().
     *
     * @param string $path
     *   Path to image.
     *
     * @return string|false
     *   The path to the new file, or FALSE in the event of an error.
     */
    protected function fileUnmanagedCopy($path)
    {
        $filename = basename($path);
        return file_unmanaged_copy($path, 'public://' . $filename, FILE_EXISTS_REPLACE);
    }

}
